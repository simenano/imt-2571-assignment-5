<?php



class DBModel{
 
    public $dom;
    protected $db;


    public function __construct(){
        try{
            $this->dom = new DOMDocument();
            $this->dom->load('../SkierLogs.xml');
            try {
                $this->db = new PDO('mysql:host=localhost; dbname=skierlogs; charset=utf8','root', '');
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $ex) {
                echo "Error ";
                echo $ex->getMessage();
            }
        }catch(Exception $ex){
            echo "Error ";
            echo $ex->getMessage();
        }
    }
	 
 
	/*Calling functions for parsing*/
	public function main(){
		$this->getclub();
		$this->getrepresenting();
		$this->getseason();
		$this->getskier();
	}
	
	/*function for skiers*/
	public function getskier(){
	 
		$uName = array(); 
		$fName = array();
		$lName	= array();
		$yOfBirth= array(); 
	 
		$skier =$this->dom->getElementsByTagName("Skier");
		$fName =$this->dom->getElementsByTagName("FirstName");
		$lName= $this->dom->getElementsByTagName("LastName");
		$yOfBirth= $this->dom->getElementsByTagName("YearOfBirth"); 
	 
	 
		foreach($uName as $Skier){
			array_push($uName, $Skier->getAttribute("userName"));
		}
		foreach($fName as $fn){
			array_push($fName, $fn->nodeValue); 
		}
		foreach($lName as $ln){
			array_push($lName, $ln->nodeValue);
		}
		foreach($yOfBirth as $yob){
			array_push($yOfBirth, $yob->nodeValue); 
		 }
		for($i=0; $i<sizeof(uName); $i++){
		 
			try{
				$input =$this->db->prepare('INSERT INTO Skier(uName,fName,lName,yOfBirth) Values(:uName, :firstName, :lastName,:YOB)');
				$stmt->bindValue(':uName', $skier->uName, PDO::PARAM_STR);
				$stmt->bindValue(':firstName', $skier->firstName, PDO::PARAM_STR);
				$stmt->bindValue(':lastName', $skier->lastName, PDO::PARAM_STR);
				$stmt->bindValue(':YOB', $skier->YOB, PDO::PARAM_STR);
		  
			}catch(Exception $ex){
				echo "Error ";
				echo $ex->getMessage();
			}
		}
	}
 
	public function getclub(){
	 
		$club = array(); 
		$name =array();
		$city	= array();
		$county= array(); 
	 
		$club =$this->dom->getElementsByTagName("Club");
		$name =$this->dom->getElementsByTagName("Name");
		$city= $this->dom->getElementsByTagName("City");
		$county= $this->dom->getElementsByTagName("County"); 
	 
	 
		foreach($club as $id){
			array_push($club, $id->getAttribute("id"));
		}
		foreach($name as $n){
			array_push($name, $n->nodeValue); 
		}
		foreach($city as $cityname){
			array_push($city, $cityname->nodeValue);
		}
		foreach($county as $co){
			array_push($county, $co->nodeValue); 
		}
		for($i=0; $i<sizeof($club); $i++){
		 
			try{
				$input =$this->db->prepare('INSERT INTO club(id,name,city,county) Values(:id, :n, :cityname,:co)');
				$stmt->bindValue(':id', $club->id, PDO::PARAM_STR);
				$stmt->bindValue(':n', $club->name, PDO::PARAM_STR);
				$stmt->bindValue(':cityname', $club->city, PDO::PARAM_STR);
				$stmt->bindValue(':co', $club->county, PDO::PARAM_STR);
		  
			}catch(Exception $ex){
				echo "Error ";
				echo $ex->getMessage();
			}
		}
	}
 
	public function getrepresenting(){
	 
		$skiername = array(); 
		$clubid = array();
		$seasonyear = array();
		$totaldist = array(); 
	 
		$skiername =$this->dom->getElementsByTagName("Skier");
		$clubid =$this->dom->getElementsByTagName("Club");
		$seasonyear= $this->dom->getElementsByTagName("Season");
		$totaldist= $this->dom->getElementsByTagName("YearOfBirth"); 
	 
	 
		foreach($skiername as $Skiernames){
			array_push($suName, $Skiernames->getAttribute("userName"));
		}
		foreach($clubid as $cid){
			array_push($clubid, $cid->getAttribute("Id")); 
		}
		foreach($seasonyear as $sy){
			array_push($seasonyear, $sy->getAttribute("fallYear"));
		}
		foreach($totaldist as $td){
			array_push($totaldist, $td->nodeValue); 
			
		}
		for($i=0; $i<sizeof($Skiername); $i++){
		 
			try{
				$input =$this->db->prepare('INSERT INTO representing(sUName,cId,sYear,totalDist) Values(:Skiername, :cid, :sy,:td)');
				$stmt->bindValue(':uName', $skier->uName, PDO::PARAM_STR);
				$stmt->bindValue(':firstName', $skier->firstName, PDO::PARAM_STR);
				$stmt->bindValue(':lastName', $skier->lastName, PDO::PARAM_STR);
				$stmt->bindValue(':YOB', $skier->YOB, PDO::PARAM_STR);
		  
			}catch(Exception $ex){
				echo "Error ";
				echo $ex->getMessage();
			}
		}
	}
 
	public function getseason(){
	 
		$year = array(); 
	 
		$year =$this->dom->getElementsByTagName("Season");
	  
	 
		foreach($year as $y){
			array_push($year, $year->getAttribute("fallYear"));
		}
		for($i=0; $i<sizeof($year); $i++){
		 
			try{
				$input =$this->db->prepare('INSERT INTO season(year) Values(:year)');
				$stmt->bindValue(':year', $year->year, PDO::PARAM_STR);
             
			}catch(Exception $ex){
				echo "Error ";
				echo $ex->getMessage();
			}
		}	
	}
 
} 
	 
 ?>